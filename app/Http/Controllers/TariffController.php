<?php

namespace App\Http\Controllers;

use App\Tariff;
use Illuminate\Http\Request;

class TariffController extends Controller
{
    public function index()
    {
        $tariffs = Tariff::all();
        return view('tariff.index', compact('tariffs'));
    }

    public function info($id)
    {
        return Tariff::findOrFail($id);
    }
}
