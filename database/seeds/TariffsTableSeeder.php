<?php

use Illuminate\Database\Seeder;

class TariffsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tariffs')->insert([
            [
                'name' => 'Базовый',
                'description' => 'Соображения высшего порядка, а также постоянное информационно-техническое обеспечение нашей деятельности играет важную роль в формировании соответствующих условий активизации!
                                    Задача организации, в особенности же начало повседневной...'
            ],
            [
                'name' => 'Стандартный',
                'description' => 'Практический опыт показывает, что новая модель организационной деятельности требует определения и уточнения новых предложений.
                                    Не следует, однако, забывать о том, что социально-экономическое развитие напрямую зависит...'
            ]
        ]);
    }
}
