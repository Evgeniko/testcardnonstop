$("#formLogin").on('submit', function (e) {
    e.preventDefault();
    $('#auth-error-span').empty();
    let csrf_token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: $(this).attr('action'),
        data: $(this).serialize(),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        dataType: 'JSON',
        success: function (resp) {
            console.log(resp);
            if (resp.status === true){
                let user = resp.user;
                $('#auth_bar').empty();
                $('#auth_bar').append(
                    '<li class="nav-item dropdown">' +
                    '<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>' +
                    user.name+'<span class="caret"></span>' +
                    '</a>' +
                    '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">' +
                    '<a class="dropdown-item" href="/logout" onclick="event.preventDefault();document.getElementById(\'logout-form\').submit();">Выйти</a>' +
                    '<form id="logout-form" action="/logout" method="POST" style="display: none;">' +
                        '<input type="hidden" name="_token" value="'+csrf_token+'">' +
                    '</form>' +
                    '</div>' +
                    '</li>'
                );
                $('#loginModal').modal('hide')
            }else {
                let errors = resp.errors;
                let errorsHTML = '';
                $.each(errors, function (i, val) {
                    errorsHTML += val+'\n'
                });
                $('#auth-error-span').html(errorsHTML);
            }
        }
    });
});

$('.choice-tariff').click(function (e) {
    let btn = $(this);
    e.preventDefault();
    if ($('.tariff-info').length){
        $('.tariff-info').empty();
    }

    let id = $(btn).attr("data-id");
    $.get('tariff/info/' + id, function (resp) {
        $('.tariff-info').append(
            '<div class="col-sm-12 text-center">'+
                '<div class="alert alert-success" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '<h4 class="alert-heading tariff-name-label">'+resp.name+'</h4>' +
                    '<p class="tariff-description-label">'+resp.description+'</p>' +
                '</div>'+
            '</div>'
        );
    });
});