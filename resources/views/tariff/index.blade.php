@extends('templates.main')
@section('content')
    <div class="container">
        <div class="row">
            @foreach ($tariffs as $tariff)
            <div class="col-sm-6">
                <div class="card text-center">
                    <div class="card-body">
                        <h5 class="card-title">{{ $tariff->name }}</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary choice-tariff" data-id="{{ $tariff->id }}">выбрать</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row tariff-info">
        </div>
    </div>
@endsection